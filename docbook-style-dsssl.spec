Name: docbook-style-dsssl
Summary: Norman Walsh's modular stylesheets for DocBook
Version: 1.79
Release: 29
License: LicenseRef-scancode-docbook and LicenseRef-scancode-iso-8879
URL: http://docbook.sourceforge.net/

Source0: http://prdownloads.sourceforge.net/docbook/docbook-dsssl-%{version}.tar.bz2
Source1: %{name}.Makefile

BuildRequires: perl-generators

Requires: docbook-dtds openjade sgml-common

Requires(post): sgml-common
Requires(preun): sgml-common

BuildArch: noarch

%description
These DSSSL stylesheets allow to convert any DocBook document to another
printed (for example, RTF or PostScript) or online (for example, HTML) format.
They are highly customizable.

%package_help

%prep
%autosetup -p1 -n docbook-dsssl-%{version}
ln -sf %{SOURCE1} Makefile

%build

%install
make install BINDIR=%{buildroot}%{_bindir} DESTDIR=%{buildroot}%{_datadir}/sgml/docbook/dsssl-stylesheets-%{version} MANDIR=%{buildroot}%{_mandir}
ln -sf ../dsssl-stylesheets-%{version} %{buildroot}%{_datadir}/sgml/docbook/dsssl-stylesheets

%post
for i in %{_sysconfdir}/sgml/*-docbook-*.cat
do
  %{_bindir}/install-catalog --add $i %{_datadir}/sgml/docbook/dsssl-stylesheets-%{version}/catalog > /dev/null 2>/dev/null
done

%preun
if [ "$1" = "0" ]; then
  for i in %{_sysconfdir}/sgml/*-docbook-*.cat
  do
    %{_bindir}/install-catalog --remove $i %{_datadir}/sgml/docbook/dsssl-stylesheets-%{version}/catalog > /dev/null 2>/dev/null
  done
fi
exit 0

%files
%{_bindir}/collateindex.pl
%{_datadir}/sgml/docbook/dsssl-stylesheets*

%files help
%doc BUGS README ChangeLog WhatsNew
%{_mandir}/man1/collateindex.pl.1*

%changelog
* Fri Sep 06 2024 xu_ping<707078654@qq.com> - 1.79-29
- License compliance rectification.

* Thu Sep 10 2020 baizhonggui<baizhonggui@huawei.com> - 1.79-28
- Replace tar.gz to tar.bz2

* Tue Dec 3 2019 caomeng<caomeng5@huawei.com> - 1.79-27
- Package init
